package transmanager.utils;


public class JSFParameterHelper {
    public static String encodeUrlParam(String param) {
        return param.replace('=', '^').replace('&', '*');
    }

    public static String decodeUrlParam(String param) {
        return param.replace('^', '=').replace('*', '&');
    }
}
