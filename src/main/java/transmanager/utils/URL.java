package transmanager.utils;


public class URL {
    private String result;
    private boolean params;
    private URL(String page) {
        result = page;
        params = false;
    }

    public static URL to(String page) {
        return new URL(page);
    }

    public static URL redTo(String page) {
        return new URL(page).redirect();
    }

    public URL redirect() {
        if(!params)
        {
            this.result += "?";
            params = true;
        }
        else
            this.result += "&";
        this.result += "faces-redirect=true";
        return this;
    }

    public String go() {
        return this.result;
    }

    public URL add(String paramName, Object paramVal) {
        if(!params) {
            this.result += "?";
            params = true;
        }
        else
            this.result += "&";
        this.result += paramName + "=" + paramVal.toString();
        return this;
    }

}
