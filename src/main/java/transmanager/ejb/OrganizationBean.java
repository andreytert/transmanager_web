package transmanager.ejb;

import transmanager.entities.Organization;
import transmanager.entities.Sanction;

import javax.ejb.Stateless;
import javax.faces.context.FacesContext;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Stateless
public class OrganizationBean extends AbstractEntityBean<Organization, Integer> {
    public OrganizationBean() {
        super(Organization.class, "getAllOrganizations");
    }

    public List<Sanction> getSanctions(int orgId) {
        Organization organization = getById(orgId);
        List<Sanction> ans = new ArrayList<Sanction>(organization.getSanctions());
        return ans;
    }

    public void addSanction(int orgId, int sanctionId) {
        String str = FacesContext.class.getPackage().getImplementationVersion();
        Organization org = getEm().find(Organization.class, orgId);
        Sanction sanction = getEm().find(Sanction.class, sanctionId);
        org.getSanctions().add(sanction);
        update(org);
    }

    public void removeSanction(int orgId, int sanctionId) {
        Organization org = getEm().find(Organization.class, orgId);
        Sanction sanction = getEm().find(Sanction.class, sanctionId);
        org.getSanctions().remove(sanction);
        update(org);
    }
}
