package transmanager.ejb;

import transmanager.entities.*;

import javax.ejb.Stateless;

@Stateless
public class RequestBean extends AbstractEntityBean<Request, Integer> {

    public RequestBean() {
        super(Request.class, "getAllRequests");
    }

    @Override
    protected void prepareToSave(Request entity) {
        if (entity.getDriver().getId() == 0)
            entity.setDriver(null);
        if (entity.getCar().getId() == 0)
            entity.setCar(null);
        if (entity.getCustomer().getId() == 0)
            entity.setCustomer(null);
        if (entity.getPerformer().getId() == 0)
            entity.setPerformer(null);
        if (entity.getShipper().getId() == 0)
            entity.setShipper(null);
    }

    @Override
    public void prepareEntity(Request entity) {
        if (entity.getDriver() == null)
            entity.setDriver(new Driver());
        if (entity.getCar() == null)
            entity.setCar(new Car());
        if (entity.getPerformer() == null)
            entity.setPerformer(new Customer());
        if (entity.getShipper() == null)
            entity.setShipper(new Transporter());
        if (entity.getCustomer() == null)
            entity.setCustomer(new Customer());
    }
}
