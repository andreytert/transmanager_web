package transmanager.ejb;

import transmanager.entities.AbstractEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.util.List;

public abstract class AbstractEntityBean<T extends AbstractEntity, PK extends Serializable> {
    @PersistenceContext
    private EntityManager em;
    private Class<T> entityClass;
    private String loadAllQueryName;

    public AbstractEntityBean() {

    }

    protected AbstractEntityBean(Class<T> entityClass, String loadAllQueryName) {
        this.loadAllQueryName = loadAllQueryName;
        this.entityClass = entityClass;
    }

    public List<T> getAll() {
        List<T> list = em.createNamedQuery(getLoadAllQueryName(), entityClass).getResultList();
        return list;
    }

    public void create(T entity) {
        prepareToSave(entity);
        em.persist(entity);
    }

    public T getById(PK id) {
        return em.find(entityClass, id);
    }

    public void delete(PK id) {
        T entity = em.find(entityClass, id);
        if (entity != null) {
            entity.setActive(false);
            em.merge(entity);
        }
    }

    public void update(T entity) {
        prepareToSave(entity);
        em.merge(entity);
    }

    protected void prepareToSave(T entity) {

    }

    protected void prepareEntity(T entity) {

    }

    public String getLoadAllQueryName() {
        return loadAllQueryName;
    }

    public void setLoadAllQueryName(String loadAllQueryName) {
        this.loadAllQueryName = loadAllQueryName;
    }

    public EntityManager getEm() {
        return em;
    }
}
