package transmanager.ejb;

import transmanager.entities.Driver;

import javax.ejb.Stateless;

@Stateless
public class DriverBean extends AbstractEntityBean<Driver, Integer> {
    public DriverBean() {
        super(Driver.class, "getAllDrivers");
    }
}
