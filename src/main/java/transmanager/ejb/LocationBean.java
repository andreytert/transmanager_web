package transmanager.ejb;


import transmanager.entities.Location;

import javax.ejb.Stateless;
import java.util.List;

@Stateless
public class LocationBean extends AbstractEntityBean<Location, Integer> {
    public LocationBean() {
        super(Location.class, null);
    }

    @Override
    public List<Location> getAll() {
        throw new UnsupportedOperationException();
    }
}
