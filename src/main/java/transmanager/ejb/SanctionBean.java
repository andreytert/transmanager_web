package transmanager.ejb;

import transmanager.entities.Sanction;

import javax.ejb.Stateless;

@Stateless
public class SanctionBean extends AbstractEntityBean<Sanction, Integer> {
    public SanctionBean() {
        super(Sanction.class, "getAllSanctions");
    }
}
