package transmanager.ejb;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import transmanager.entities.Request;
import transmanager.xmladaptor.CalendarAdaptor;

import javax.ejb.Stateless;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Starik on 12.11.13.
 */


@Stateless
public class RequestExportExelDocumentBean {

    @PersistenceContext
    EntityManager em;

    public void createAndExportReports() throws Exception {

        byte[] result = create();
        HttpServletResponse response =
                (HttpServletResponse) FacesContext.getCurrentInstance()
                        .getExternalContext().getResponse();

        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment;filename=document.xlsx");

        response.getOutputStream().write(result);
        response.getOutputStream().flush();
        response.getOutputStream().close();
        FacesContext.getCurrentInstance().responseComplete();
    }

    byte[] create() throws Exception {
        String input_DOCX = RequestDocumentBean.class.getClassLoader().getResource("reestrTemplate.xlsx").getPath();
        InputStream inpt = new FileInputStream(input_DOCX);

        XSSFWorkbook workbook = new XSSFWorkbook(inpt);
        XSSFSheet sheet = workbook.getSheet("Реестр");
        XSSFRow row;

        Map<String, CellStyle> styles = mkStyles(workbook);
        List<Request> allRec = em.createNamedQuery("getAllRequests", Request.class).getResultList();

        int rowCount = 2;
        for (Request rec : allRec) {
            sheet.createRow(rowCount);
            row = sheet.getRow(rowCount++);

            //row.createCell(0).setCellValue(rec.getNumber().isEmpty()?null:rec.getNumber());
            //row.createCell(1).setCellValue("1");
            CalendarAdaptor cldad = new CalendarAdaptor();


            row.createCell(2).setCellValue(rec.getNumber());
            row.createCell(3).setCellStyle(styles.get("cel_d"));
            row.getCell(3).setCellValue(cldad.marshalData(rec.getLoadingTime()));
            row.createCell(4).setCellValue(cldad.marshalTime(rec.getLoadingTime()));
            if (null != rec.getDriver()) {
                row.createCell(7).setCellValue(rec.getDriver().getName());
                row.createCell(8).setCellValue(rec.getDriver().getPhoneNumber());
            }
            if (null != rec.getCar()) {
                row.createCell(9).setCellValue(rec.getCar().getCarBrand());
                row.createCell(10).setCellValue(rec.getCar().getNumber());
                row.createCell(11).setCellValue(rec.getCar().getTrailerNumber());
                row.createCell(12).setCellStyle(styles.get("cell_int"));
                row.getCell(12).setCellValue(null != rec.getCar().getTonnage() ? rec.getCar().getTonnage().intValue() : null);
                row.createCell(13).setCellValue(rec.getCar().getType());
            }
            row.createCell(17).setCellStyle(styles.get("cell_int"));
            row.getCell(17).setCellValue(null != rec.getCost() ? rec.getCost().toString() : null);
            row.createCell(19).setCellFormula("S" + rowCount + "-R" + rowCount);
            row.createCell(20).setCellStyle(styles.get("cell_%"));
            row.getCell(20).setCellFormula("T" + rowCount + "/S" + rowCount);
            row.createCell(21).setCellStyle(styles.get("cell_d"));
            /*
            if(null!=rec.getPerformer()){
                row.getCell(21).setCellFormula("D" + rowCount + "+" + rec.getPerformer().getRejectDocumentDays());
                row.createCell(25).setCellStyle(styles.get("cell_d"));
                row.getCell(25).setCellFormula("Y" + rowCount + "+" + rec.getPerformer().getPaymentDayCount());
                row.createCell(27).setCellStyle(styles.get("cell_d"));
                row.getCell(27).setCellFormula("W" + rowCount + "+" + rec.getPerformer().getPaymentDayCount());
            }
            */
            row.createCell(31).setCellStyle(styles.get("cell_int"));
            row.createCell(31).setCellFormula("R" + rowCount + "-AE" + rowCount);


        }
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        workbook.write(out);
        return out.toByteArray();
    }

    private Map<String, CellStyle> mkStyles(XSSFWorkbook workbook) {
        Map<String, CellStyle> styles = new HashMap<String, CellStyle>();
        CellStyle style;

        style = workbook.createCellStyle();
        style.setDataFormat(workbook.createDataFormat().getFormat("0.000%"));
        styles.put("cell_%", style);

        style = workbook.createCellStyle();
        style.setDataFormat(workbook.createDataFormat().getFormat("dd.mm.yyyy"));
        styles.put("cell_d", style);

        style = workbook.createCellStyle();
        style.setDataFormat(workbook.createDataFormat().getFormat("0"));
        styles.put("cell_int", style);

        return styles;
    }

}
