package transmanager.ejb;

import transmanager.entities.Car;

import javax.ejb.Stateless;

@Stateless
public class CarBean extends AbstractEntityBean<Car, Integer> {
    public CarBean() {
        super(Car.class, "getAllCars");
    }
}


