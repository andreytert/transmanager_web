package transmanager.ejb;

import transmanager.entities.Customer;
import transmanager.entities.Route;

import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class CustomerBean extends AbstractEntityBean<Customer, Integer> {
    public CustomerBean() {
        super(Customer.class, "getAllCustomers");
    }

    public List<Route> getRoutes(int customerId) {
        Customer customer = getById(customerId);
        List<Route> ans = customer.getRoutes();
        return new ArrayList<Route>(ans);
    }

    public void addRoute(int customerId, Route newRoute) {
        Customer customer = getById(customerId);
        customer.getRoutes().add(newRoute);
        update(customer);
    }

    public void removeRoute(int customerId, int routeId) {
        Customer customer = getById(customerId);
        Route route = getEm().find(Route.class, routeId);
        customer.getRoutes().remove(route);
    }

}
