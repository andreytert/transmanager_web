package transmanager.ejb;

import transmanager.entities.Route;

import javax.ejb.Stateless;
import javax.naming.OperationNotSupportedException;
import java.util.List;

@Stateless
public class RouteBean extends AbstractEntityBean<Route, Integer> {
    public RouteBean() {
        super(Route.class, null);
    }

    @Override
    public List<Route> getAll() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(Integer id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void create(Route entity) {
        throw new UnsupportedOperationException();
    }
}
