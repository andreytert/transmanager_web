package transmanager.ejb;

import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.io.SaveToZipFile;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.CustomXmlDataStoragePart;
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;
import org.docx4j.utils.SingleTraversalUtilVisitorCallback;
import org.docx4j.utils.TraversalUtilVisitor;
import org.docx4j.wml.SdtElement;
import transmanager.entities.Request;

import javax.ejb.Stateless;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.List;

@Stateless
public class RequestDocumentBean {
    @PersistenceContext
    EntityManager em;

    public void createAndSendReport(int requestId) throws Exception {
        Request request = em.find(Request.class, requestId);
        if (request != null) {
            byte[] result = create(request);
            HttpServletResponse response =
                    (HttpServletResponse) FacesContext.getCurrentInstance()
                            .getExternalContext().getResponse();

            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition", "attachment;filename=document.docx");

            response.getOutputStream().write(result);
            response.getOutputStream().flush();
            response.getOutputStream().close();
            FacesContext.getCurrentInstance().responseComplete();
        }
    }


    JAXBContext context = org.docx4j.jaxb.Context.jc;

    private final boolean SAVE = true;

    byte[] create(Request request) throws Exception {

        // the docx 'template'
        String input_DOCX = RequestDocumentBean.class.getClassLoader().getResource("requestTemplate.docx").getPath();

        // Load input_template.docx
        WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.load(
                new java.io.File(input_DOCX));

        // Find custom xml item id
        String itemId = getCustomXmlItemId(wordMLPackage).toLowerCase();
        System.out.println("Looking for item id: " + itemId);

        // Inject data_file.xml
        CustomXmlDataStoragePart customXmlDataStoragePart
                = wordMLPackage.getCustomXmlDataStorageParts().get(itemId);
        if (customXmlDataStoragePart == null) {
            System.out.println("Couldn't find CustomXmlDataStoragePart! exiting..");
            return null;
        }

        StringWriter writer = new StringWriter();
        JAXBContext context = JAXBContext.newInstance(Request.class);
        Marshaller m = context.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        m.marshal(request, writer);

        InputStream inputStream = new ByteArrayInputStream(writer.toString().getBytes("UTF-8"));
        customXmlDataStoragePart.getData().setDocument(inputStream);

        SaveToZipFile saver = new SaveToZipFile(wordMLPackage);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        saver.save(out);
        return out.toByteArray();
    }


    private String getCustomXmlItemId(WordprocessingMLPackage wordMLPackage) throws Docx4JException {

        MainDocumentPart documentPart = wordMLPackage.getMainDocumentPart();
        if (wordMLPackage.getMainDocumentPart().getXPathsPart() == null) {
            // Can't do it the easy way, so look up the binding on the first content control
            TraversalUtilCCVisitor visitor = new TraversalUtilCCVisitor();
            SingleTraversalUtilVisitorCallback ccFinder
                    = new SingleTraversalUtilVisitorCallback(visitor);
            ccFinder.walkJAXBElements(
                    wordMLPackage.getMainDocumentPart().getJaxbElement().getBody());
            return visitor.storeItemID;

        } else {

            org.opendope.xpaths.Xpaths xPaths = wordMLPackage.getMainDocumentPart().getXPathsPart().getJaxbElement();
            return xPaths.getXpath().get(0).getDataBinding().getStoreItemID();
        }
    }

    public static class TraversalUtilCCVisitor extends TraversalUtilVisitor<SdtElement> {

        String storeItemID = null;

        @Override
        public void apply(SdtElement element, Object parent, List<Object> siblings) {

            if (element.getSdtPr() != null
                    && element.getSdtPr().getDataBinding() != null) {
                storeItemID = element.getSdtPr().getDataBinding().getStoreItemID();
            }
        }

    }
}
