package transmanager.ejb;

import transmanager.entities.Transporter;

import javax.ejb.Stateless;

@Stateless
public class TransporterBean extends AbstractEntityBean<Transporter, Integer> {
    public TransporterBean() {
        super(Transporter.class, "getAllTransporters");
    }
}
