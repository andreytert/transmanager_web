package transmanager.controllers;

import transmanager.ejb.DriverBean;
import transmanager.entities.Driver;
import transmanager.utils.URL;

import javax.ejb.EJB;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

@Named
@ViewScoped
public class DriverController {

    @EJB
    DriverBean driverBean;

    private Driver currentDriver;

    public DriverController() {
        this.currentDriver = new Driver();
    }

    //region Methods
    public String doCreate() {
        driverBean.create(currentDriver);
        return URL.redTo("drivers").go();
    }

    public String update() {
        driverBean.update(currentDriver);
        return URL.redTo("drivers").go();
    }

    public void init(ComponentSystemEvent event) throws AbortProcessingException {
        currentDriver = driverBean.getById(currentDriver.getId());
    }
    //endregion

    //region Getters and Setters
    public Driver getCurrentDriver() {
        if (currentDriver == null)
            currentDriver = new Driver();
        return currentDriver;
    }

    public void setCurrentDriver(Driver currentDriver) {
        this.currentDriver = currentDriver;
    }
    //endregion
}
