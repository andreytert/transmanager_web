package transmanager.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import transmanager.ejb.AbstractEntityBean;
import transmanager.ejb.CarBean;
import transmanager.entities.Car;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

@Named
@ViewScoped
public class CarsController extends AbstractEntitiesController<Car> {
    @EJB
    private CarBean carBean;
    private static Logger logger = LogManager.getLogger(CarsController.class.getName());

    public CarsController() {
        super("cars", "editCar");
    }

    @Override
    protected AbstractEntityBean getBean() {
        return carBean;
    }
}