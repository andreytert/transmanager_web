package transmanager.controllers;


import transmanager.ejb.LocationBean;
import transmanager.entities.Location;
import transmanager.utils.JSFParameterHelper;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

@Named
@ViewScoped
public class LocationController {

    @EJB
    LocationBean locationBean;

    Location currentLocation;
    String url;

    LocationController() {
        currentLocation = new Location();
    }

    //region Methods
    public String update() {
        locationBean.update(currentLocation);
        return JSFParameterHelper.decodeUrlParam(url);
    }

    public void init() {
        currentLocation = locationBean.getById(currentLocation.getId());
    }
    //endregion

    //region Getters and Setters
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Location getCurrentLocation() {
        return currentLocation;
    }

    public void setCurrentLocation(Location currentLocation) {
        this.currentLocation = currentLocation;
    }
    //endregion
}
