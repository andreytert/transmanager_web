package transmanager.controllers;

import transmanager.ejb.AbstractEntityBean;
import transmanager.ejb.DriverBean;
import transmanager.entities.Driver;
import transmanager.utils.URL;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.util.List;

@Named
@ViewScoped
public class DriversController extends AbstractEntitiesController<Driver>{
    @EJB
    private DriverBean driverBean;

    public DriversController() {
        super("drivers", "editDriver");
    }

    //region Methods
    public String deleteDriver(int id) {
        driverBean.delete(id);
        return URL.redTo("drivers").go();
    }

    public String editDriver(int id) {
        return URL.redTo("editDriver").add("id",id).go();
    }
    //endregion

    //region Getters and Setters
    public int getDriversCount() {
        return driverBean.getAll().size();
    }

    @Override
    protected AbstractEntityBean getBean() {
        return null;
    }

    public List<Driver> getAllDrivers() {
        return driverBean.getAll();
    }

    //endregion
}
