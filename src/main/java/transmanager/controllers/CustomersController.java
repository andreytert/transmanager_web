package transmanager.controllers;

import transmanager.ejb.AbstractEntityBean;
import transmanager.ejb.CustomerBean;
import transmanager.entities.Customer;
import transmanager.entities.Route;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.util.List;

@Named
@ViewScoped
public class CustomersController extends AbstractEntitiesController<Customer> {
    @EJB
    private CustomerBean customerBean;

    public CustomersController() {
        super("customers", "editCustomer");
    }

    @Override
    protected AbstractEntityBean getBean() {
        return customerBean;
    }
}
