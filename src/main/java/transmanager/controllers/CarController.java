package transmanager.controllers;

import transmanager.ejb.CarBean;
import transmanager.entities.Car;
import transmanager.utils.URL;

import javax.ejb.EJB;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

@Named
@ViewScoped
public class CarController {
    @EJB
    CarBean carBean;

    private Car currentCar;

    public CarController() {
        this.currentCar = new Car();
    }

    //region Methods
    public String doCreate() {
        carBean.create(currentCar);
        return URL.redTo("cars").go();
    }

    public String update() {
        carBean.update(currentCar);
        return URL.redTo("cars").go();
    }

    public void init(ComponentSystemEvent event) throws AbortProcessingException {
        currentCar = carBean.getById(currentCar.getId());
    }
    //endregion

    //region Grtters and Setters
    public Car getCurrentCar() {
        if (currentCar == null)
            currentCar = new Car();
        return currentCar;
    }

    public void setCurrentCar(Car currentCar) {
        this.currentCar = currentCar;
    }
    //endregion

}
