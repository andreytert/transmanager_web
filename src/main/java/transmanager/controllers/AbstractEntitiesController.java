package transmanager.controllers;


import transmanager.ejb.AbstractEntityBean;
import transmanager.entities.AbstractEntity;
import transmanager.entities.Car;
import transmanager.utils.URL;

import java.util.List;

public abstract class AbstractEntitiesController<T extends AbstractEntity> {
    private String page;
    private String editPage;
    protected abstract AbstractEntityBean getBean();

    public AbstractEntitiesController() {

    }

    protected AbstractEntitiesController(String page, String editPage) {
        this.page = page;
        this.editPage = editPage;
    }

    public List<T> getAll() {
        return getBean().getAll();
    }

    public int getCount() {
        return getBean().getAll().size();
    }

    public String delete(int id) {
        getBean().delete(id);
        return URL.redTo(page).go();
    }

    public String edit(int id) {
        return URL.redTo(editPage).add("id", id).go();
    }
}
/*


 */