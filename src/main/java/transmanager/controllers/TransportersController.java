package transmanager.controllers;

import transmanager.ejb.AbstractEntityBean;
import transmanager.ejb.TransporterBean;
import transmanager.entities.Transporter;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

@Named
@ViewScoped
public class TransportersController extends AbstractEntitiesController<Transporter> {
    @EJB
    private TransporterBean transporterBean;

    public TransportersController() {
        super("transporters", "editTransporter");
    }

    @Override
    protected AbstractEntityBean getBean() {
        return transporterBean;
    }
}
