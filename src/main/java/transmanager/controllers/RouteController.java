package transmanager.controllers;

import transmanager.ejb.CustomerBean;
import transmanager.ejb.RouteBean;
import transmanager.entities.Route;
import transmanager.utils.URL;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.util.HashMap;
import java.util.Map;

@Named
@ViewScoped
public class RouteController {
    @EJB
    private CustomerBean customerBean;

    @EJB
    private RouteBean routeBean;

    private Route current;
    private int customerId;

    public RouteController() {
        current = new Route();
    }

    public void removeRoute(int customerId, int routeId) {
        customerBean.removeRoute(customerId, routeId);
        URL.redTo("customerRoutes").add("id", customerId).go();
    }

    public String update() {
        routeBean.update(current);
        return URL.redTo("customerRoutes").add("id", customerId).go();
    }

    public void init() {
        if(current.getId() != 0)
            current = routeBean.getById(current.getId());
    }

    public String create() {
        customerBean.addRoute(customerId, current);
        return URL.redTo("customerRoutes").add("id", customerId).go();
    }

    public Route getCurrent() {
        return current;
    }

    public void setCurrent(Route current) {
        this.current = current;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }
}
