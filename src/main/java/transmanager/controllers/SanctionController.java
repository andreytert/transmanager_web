package transmanager.controllers;

import transmanager.ejb.SanctionBean;
import transmanager.entities.Sanction;
import transmanager.utils.URL;

import javax.ejb.EJB;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

@Named
@ViewScoped
public class SanctionController {
    @EJB
    SanctionBean sanctionBean;

    private Sanction currentSanction;

    public SanctionController() {
        this.currentSanction = new Sanction();
    }

    //region Methods
    public String doCreate() {
        sanctionBean.create(currentSanction);
        return URL.redTo("sanctions").go();
    }

    public String update() {
        sanctionBean.update(currentSanction);
        return URL.redTo("sanctions").go();
    }

    public void init(ComponentSystemEvent event) throws AbortProcessingException {
        currentSanction = sanctionBean.getById(currentSanction.getId());
    }
    //endregion

    //region Getters and Setters
    public Sanction getCurrentSanction() {
        return currentSanction;
    }

    public void setCurrentSanction(Sanction currentSanction) {
        this.currentSanction = currentSanction;
    }
    //endregion
}
