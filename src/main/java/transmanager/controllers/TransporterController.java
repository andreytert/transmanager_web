package transmanager.controllers;

import transmanager.ejb.OrganizationBean;
import transmanager.ejb.TransporterBean;
import transmanager.entities.Transporter;
import transmanager.utils.URL;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

@Named
@ViewScoped
public class TransporterController {

    @EJB
    private TransporterBean transporterBean;

    @EJB
    private OrganizationBean organizationBean;

    private Transporter currentTransporter;

    public TransporterController() {
        currentTransporter = new Transporter();
    }

    //region Methods
    public String toSanctions(){
        return URL.redTo("organizationSanctions").add("id", currentTransporter.getOrg().getId()).go();
    }

    public String doCreate() {
        transporterBean.create(currentTransporter);
        return URL.redTo("transporters").go();
    }

    public String update() {
        transporterBean.update(currentTransporter);
        return URL.redTo("transporters").go();
    }

    public void init(){
        currentTransporter = transporterBean.getById(currentTransporter.getId());
    }
    //endregion

    //region Getters and Setters
    public Transporter getCurrentTransporter() {
        return currentTransporter;
    }

    public void setCurrentTransporter(Transporter currentTransporter) {
        this.currentTransporter = currentTransporter;
    }
    //endregion

}
