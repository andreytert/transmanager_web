package transmanager.controllers;

import transmanager.ejb.OrganizationBean;
import transmanager.entities.Location;
import transmanager.entities.Organization;
import transmanager.entities.Sanction;
import transmanager.utils.URL;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.util.List;

@Named
@ViewScoped
public class OrganizationController {
    @EJB
    OrganizationBean organizationBean;

    private Organization current;

    public OrganizationController() {
        current = new Organization();
    }

    //region Methods
    public String addSanction(int sanctionId) {
        organizationBean.addSanction(current.getId(), sanctionId);
        return URL.redTo("organizationSanctions").add("id", current.getId()).go();
    }

    public String removeSanction(int sanctionId) {
        organizationBean.removeSanction(current.getId(), sanctionId);
        return URL.redTo("organizationSanctions").add("id", current.getId()).go();
    }

    public void init()  {
        current = organizationBean.getById(current.getId());
    }
    //endregion

    //region Getters and Setters
    public List<Sanction> getSanctions() {

        return organizationBean.getSanctions(current.getId());
    }

    public List<Location> getUnloadingAddresses() {
        return null;
    }

    public Organization getCurrent() {
        return current;
    }

    public void setCurrent(Organization current) {
        this.current = current;
    }
    //endregion
}
