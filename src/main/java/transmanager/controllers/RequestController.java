package transmanager.controllers;

import org.docx4j.openpackaging.exceptions.Docx4JException;
import transmanager.ejb.*;
import transmanager.entities.Location;
import transmanager.entities.Request;
import transmanager.utils.URL;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.xml.bind.JAXBException;

@Named
@ViewScoped
public class RequestController{
    @EJB
    RequestBean requestBean;

    @EJB
    CustomerBean customerBean;

    @EJB
    TransporterBean transporterBean;

    private Request current;

    public RequestController() {
        this.current = new Request();
    }

    //region Methods
    public String doCreate() throws JAXBException, Docx4JException {
        requestBean.create(current);
        return URL.redTo("requests").go();
    }

    public String update() {
        requestBean.update(current);
        return URL.redTo("requests").go();
    }

    public void init() {
        current = requestBean.getById(current.getId());
        requestBean.prepareEntity(current);
    }
    //endregion

    public Request getCurrent() {
        if (current == null)
            current = new Request();
        return current;
    }

    public void setCurrent(Request current) {
        this.current = current;
    }
    //endregion


}