package transmanager.controllers;

import transmanager.ejb.AbstractEntityBean;
import transmanager.ejb.SanctionBean;
import transmanager.entities.Sanction;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

@Named
@ViewScoped
public class SanctionsController extends AbstractEntitiesController<Sanction>{
    @EJB
    private SanctionBean sanctionBean;

    public SanctionsController() {
        super("sanctions","editSanction");
    }

    //region Getters and Setters
    @Override
    protected AbstractEntityBean getBean() {
        return sanctionBean;
    }

    //endregion
}
