package transmanager.controllers;

import transmanager.ejb.AbstractEntityBean;
import transmanager.ejb.RequestBean;
import transmanager.ejb.RequestDocumentBean;
import transmanager.ejb.RequestExportExelDocumentBean;
import transmanager.entities.Request;
import transmanager.utils.URL;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.util.List;

@Named
@ViewScoped
public class RequestsController extends AbstractEntitiesController<Request> {
    @EJB
    private RequestBean requestBean;

    @EJB
    private RequestDocumentBean requestDocumentBean;

    @EJB
    private RequestExportExelDocumentBean requestExportExelDocumentBean;

    //region Methods
    public String deleteRequest(int id) {
        requestBean.delete(id);
        return URL.redTo("requests").go();
    }

    public String editRequest(int id) {
        return URL.redTo("editRequest").add("id",id).go();
    }

    public void downloadDocument(int id) throws Exception {
        requestDocumentBean.createAndSendReport(id);
    }

    public void exportExcelDocument() throws Exception {
        requestExportExelDocumentBean.createAndExportReports();
    }
    //endregion

    //region Getters and Setters

    @Override
    protected AbstractEntityBean getBean() {
        return requestBean;
    }
    //endregion

}
