package transmanager.controllers;

import transmanager.ejb.CustomerBean;
import transmanager.entities.Customer;
import transmanager.entities.Route;
import transmanager.utils.URL;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

@Named
@ViewScoped
public class CustomerController {
    @EJB
    private CustomerBean customerBean;

    private Customer current;

    public CustomerController() {
        current = new Customer();
    }

    //region Methods
    public String doCreate() {
        customerBean.create(current);
        return URL.redTo("customers").go();
    }

    public String update() {
        customerBean.update(current);
        return URL.redTo("customers").go();
    }

    public String toSanctions() {
        return URL.redTo("organizationSanctions").add("id", current.getOrg().getId()).go();
    }

    public String toRoutes() {
        return URL.redTo("customerRoutes").add("id", current.getId()).go();
    }

    public String toCreateRoute() {
        return URL.redTo("createRoute").add("customerId", current.getId()).go();
    }

    public String toEditRoute(int routeId) {
        return URL.redTo("editRoute").add("id", routeId).add("customerId", current.getId()).go();
    }

    public List<Route> getRoutes(Integer customerId) {
        if(customerId != null)
            return customerBean.getRoutes(customerId);
        return new ArrayList<Route>();
    }

    public List<Route> getRoutes() {
        return customerBean.getRoutes(current.getId());
    }

    public void init() {
        current = customerBean.getById(current.getId());
    }
    //endregion

    //region Getters and Setters
    public Customer getCurrent() {
        return current;
    }

    public void setCurrent(Customer current) {
        this.current = current;
    }
    //endregion
}
