package transmanager.xmladaptor;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class CalendarAdaptor extends XmlAdapter<String, Calendar> {

    @Override
    public String marshal(Calendar v) throws Exception {
        SimpleDateFormat fmt = new SimpleDateFormat("«dd» MMMMMMM yyyyг. HH:mm", new Locale("ru"));
        return fmt.format(v.getTime());
    }

    @Override
    public Calendar unmarshal(String v) throws Exception {
        return null;
    }

    public String marshalData(Calendar v) throws Exception {
        SimpleDateFormat smp = new SimpleDateFormat("dd.MM.yyyy", new Locale("ru"));
        if (null != v)
            return smp.format(v.getTime());
        else
            return null;
    }

    public String marshalTime(Calendar v) throws Exception {
        SimpleDateFormat smp = new SimpleDateFormat("HH:mm", new Locale("ru"));
        if (null != v)
            return smp.format(v.getTime());
        else
            return null;
    }

}