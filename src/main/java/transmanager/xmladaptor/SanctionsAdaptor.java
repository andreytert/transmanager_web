package transmanager.xmladaptor;


import transmanager.entities.Sanction;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.util.List;
import java.util.Set;

public class SanctionsAdaptor extends XmlAdapter<String, Set<Sanction>> {
    @Override
    public Set<Sanction> unmarshal(String s) throws Exception {
        return null;
    }

    @Override
    public String marshal(Set<Sanction> sanctions) throws Exception {
        StringBuilder sb = new StringBuilder();
        boolean first = true;
        for(Sanction sanction : sanctions) {
            sb.append((first ? "" : "\n") + "\t - " + sanction.getSanctionText());
            first = false;
        }
        return sb.toString();
    }
}
