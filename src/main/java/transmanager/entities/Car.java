package transmanager.entities;

import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import java.math.BigDecimal;

@NamedQuery(name = "getAllCars", query = "select d from Car d where  d.active = true")
@Entity
public class Car extends AbstractEntity {

    String carBrand;
    String number;
    String trailerNumber;
    String techInspect;
    String type;
    BigDecimal tonnage;
    BigDecimal capacity;
    BigDecimal volume;
    String comment;

    public String getCarBrand() {
        return carBrand;
    }

    public void setCarBrand(String carBrand) {
        this.carBrand = carBrand;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getTrailerNumber() {
        return trailerNumber;
    }

    public void setTrailerNumber(String trailerNumber) {
        this.trailerNumber = trailerNumber;
    }

    public String getTechInspect() {
        return techInspect;
    }

    public void setTechInspect(String techInspect) {
        this.techInspect = techInspect;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BigDecimal getTonnage() {
        return tonnage;
    }

    public void setTonnage(BigDecimal tonnage) {
        this.tonnage = tonnage;
    }

    public BigDecimal getCapacity() {
        return capacity;
    }

    public void setCapacity(BigDecimal capacity) {
        this.capacity = capacity;
    }

    public BigDecimal getVolume() {
        return volume;
    }

    public void setVolume(BigDecimal volume) {
        this.volume = volume;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
