package transmanager.entities;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

@NamedQuery(name = "getAllTransporters", query = "select d from Transporter  d where d.active = true")
@Entity
public class Transporter extends AbstractEntity {
    @OneToOne(cascade = CascadeType.ALL)
    private Organization org;

    public Transporter() {
        org = new Organization();
    }

    //region getters and setters

    public Organization getOrg() {
        return org;
    }

    public void setOrg(Organization org) {
        this.org = org;
    }
    //endregion
}
