package transmanager.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@NamedQuery(name = "getAllCustomers", query = "select d from Customer d where d.active = true")
@Entity
public class Customer extends AbstractEntity {
    @OneToOne(cascade = CascadeType.ALL)
    private Organization org;
    @OneToMany(cascade = CascadeType.ALL)
    private List<Route> routes;

    public Customer() {
        routes = new ArrayList<Route>();
        org = new Organization();
    }

    //region getters and setters
    public Organization getOrg() {
        return org;
    }

    public void setOrg(Organization org) {
        this.org = org;
    }

    public List<Route> getRoutes() {
        return routes;
    }

    public void setRoutes(List<Route> routes) {
        this.routes = routes;
    }
    //endregion
}
