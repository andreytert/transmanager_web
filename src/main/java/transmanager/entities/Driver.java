package transmanager.entities;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@NamedQuery(name = "getAllDrivers", query = "select d from Driver d where d.active = true")
@Entity
public class Driver extends AbstractEntity {
    @Size(min = 1, message = "{no_empty_str}")
    String name;

    @Size(min = 1, message = "{no_empty_str}")
    String driverLicense;

    @Pattern(regexp = "[[0-9]{1,3}-[0-9]{1,3}]+")
    String phoneNumber;

    @Size(min = 1, message = "{no_empty_str}")
    String passportDetails;

    @OneToOne(cascade = CascadeType.ALL)
    Location registrationAdress;

    @OneToOne(cascade = CascadeType.ALL)
    Location realAdress;

    public Driver() {
        realAdress = new Location();
        registrationAdress = new Location();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDriverLicense() {
        return driverLicense;
    }

    public void setDriverLicense(String driverLicense) {
        this.driverLicense = driverLicense;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPassportDetails() {
        return passportDetails;
    }

    public void setPassportDetails(String passportDetails) {
        this.passportDetails = passportDetails;
    }

    public Location getRegistrationAdress() {
        return registrationAdress;
    }

    public void setRegistrationAdress(Location registrationAdress) {
        this.registrationAdress = registrationAdress;
    }

    public Location getRealAdress() {
        return realAdress;
    }

    public void setRealAdress(Location realAdress) {
        this.realAdress = realAdress;
    }
}
