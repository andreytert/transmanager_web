package transmanager.entities;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.GregorianCalendar;

@XmlRootElement
@NamedQuery(name = "getAllRequests", query = "select d from Request d where d.active = true")
@Entity
public class Request extends AbstractEntity {
    String number;
    @Temporal(TemporalType.TIMESTAMP)
    Calendar Date;
    String responsiblePersonName;
    String responsiblePersonPhone;
    String contractName;
    String contractNumber;
    @Temporal(TemporalType.TIMESTAMP)
    Calendar loadingTime;
    @Temporal(TemporalType.TIMESTAMP)
    Calendar unloadingTime;
    @Temporal(TemporalType.TIMESTAMP)
    Calendar currentDate;
    BigDecimal cost;

    @Column(name = "driver_id")
    private Integer driverId;
    @OneToOne()
    @PrimaryKeyJoinColumn(name = "driver_id")
    Driver driver;

    @Column(name = "car_id")
    private Integer carId;
    @OneToOne
    @PrimaryKeyJoinColumn(name = "car_id")
    private Car car;

    @Column(name = "customer_id")
    private Integer customerId;
    @OneToOne()
    @PrimaryKeyJoinColumn()
    private Customer customer;

    @Column(name = "performer_id")
    private Integer performerId;
    @OneToOne
    @PrimaryKeyJoinColumn(name = "performer_id")
    private Customer performer;

    @Column(name = "shipper_id")
    private Integer shipperId;
    @OneToOne
    @PrimaryKeyJoinColumn(name = "shipper_id")
    private Transporter shipper;

    @Column(name = "route_id")
    private Integer routeId;
    @OneToOne
    @PrimaryKeyJoinColumn(name = "route_id")
    private Route route;


    public Request() {
        driver = new Driver();
        car = new Car();
        customer = new Customer();
        performer = new Customer();
        shipper = new Transporter();
        loadingTime = new GregorianCalendar();
        unloadingTime = new GregorianCalendar();
        currentDate = new GregorianCalendar();
    }

    //region getters and setters
    public Calendar getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(Calendar currentDate) {
        this.currentDate = currentDate;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Calendar getDate() {
        return Date;
    }

    public void setDate(Calendar date) {
        Date = date;
    }

    public String getResponsiblePersonName() {
        return responsiblePersonName;
    }

    public void setResponsiblePersonName(String responsiblePersonName) {
        this.responsiblePersonName = responsiblePersonName;
    }

    public String getResponsiblePersonPhone() {
        return responsiblePersonPhone;
    }

    public void setResponsiblePersonPhone(String responsiblePersonPhone) {
        this.responsiblePersonPhone = responsiblePersonPhone;
    }

    public String getContractName() {
        return contractName;
    }

    public void setContractName(String contractName) {
        this.contractName = contractName;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public Calendar getLoadingTime() {
        return loadingTime;
    }

    public void setLoadingTime(Calendar loadingTime) {
        this.loadingTime = loadingTime;
    }

    public Calendar getUnloadingTime() {
        return unloadingTime;
    }

    public void setUnloadingTime(Calendar unloadingTime) {
        this.unloadingTime = unloadingTime;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Customer getPerformer() {
        return performer;
    }

    public void setPerformer(Customer performer) {
        this.performer = performer;
    }

    public Transporter getShipper() {
        return shipper;
    }

    public void setShipper(Transporter shipper) {
        this.shipper = shipper;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Integer getRouteId() {
        return routeId;
    }

    public void setRouteId(Integer routeId) {
        this.routeId = routeId;
    }

    public Integer getDriverId() {
        return driverId;
    }

    public void setDriverId(Integer driverId) {
        this.driverId = driverId;
    }

    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    public Integer getPerformerId() {
        return performerId;
    }

    public void setPerformerId(Integer performerId) {
        this.performerId = performerId;
    }

    public Integer getShipperId() {
        return shipperId;
    }

    public void setShipperId(Integer shipperId) {
        this.shipperId = shipperId;
    }

    public Route getRoute() {
        return route;
    }

    public void setRoute(Route route) {
        this.route = route;
    }

    //endregion
}
