@XmlJavaTypeAdapter(value = CalendarAdaptor.class, type = Calendar.class)
@XmlAccessorType(XmlAccessType.FIELD)
package transmanager.entities;

import transmanager.xmladaptor.CalendarAdaptor;
import transmanager.xmladaptor.SanctionsAdaptor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.Calendar;
import java.util.List;