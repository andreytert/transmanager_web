package transmanager.entities;

import javax.persistence.Entity;
import javax.persistence.NamedQuery;

/**
 * Created by luckyi on 07.11.13.
 */

@NamedQuery(name = "getAllSanctions", query = "select s from Sanction s where  s.active = true")
@Entity
public class Sanction extends AbstractEntity {

    String sanctionText;

    //@ManyToOne(cascade = CascadeType.ALL, fetch=FetchType.LAZY)
    //@JoinColumn(name="OWNER_ID")
    //private Request owner;
    //@ManyToMany
    //Set<Contragent> contragents = new HashSet<Contragent>();


    public String getSanctionText() {
        return this.sanctionText;
    }

    public void setSanctionText(String sanctionText) {
        this.sanctionText = sanctionText;
    }

    /*public void setOwner(Request owner) {
        this.owner = owner;
    }

    public Request getOwner() {
        return this.owner;
    }*/
}
