package transmanager.entities;

import transmanager.xmladaptor.SanctionsAdaptor;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@NamedQuery(name = "getAllOrganizations", query = "select d from Organization d where d.active = true")
public class Organization extends AbstractEntity {
    private String name;
    private String phoneNumber;
    private String email;
    private String webSite;
    private String director;
    private String contactName;

    @OneToOne(cascade = CascadeType.ALL)
    Location postalAddress;

    @OneToOne(cascade = CascadeType.ALL)
    Location juridAddress;

    @OneToOne(cascade = CascadeType.ALL)
    Bankinfo bankinfo;

    @XmlElement
    @XmlJavaTypeAdapter(SanctionsAdaptor.class)
    @ManyToMany
    Set<Sanction> sanctions = new HashSet<Sanction>();

    public Organization() {
        postalAddress = new Location();
        juridAddress = new Location();
        bankinfo = new Bankinfo();
    }

    //region getters and setters

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String telephone) {
        this.phoneNumber = telephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebSite() {
        return webSite;
    }

    public void setWebSite(String webSite) {
        this.webSite = webSite;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public Location getPostalAddress() {
        return postalAddress;
    }

    public void setPostalAddress(Location postalAddress) {
        this.postalAddress = postalAddress;
    }

    public Location getJuridAddress() {
        return juridAddress;
    }

    public void setJuridAddress(Location juridAddress) {
        this.juridAddress = juridAddress;
    }

    public Bankinfo getBankinfo() {
        return bankinfo;
    }

    public void setBankinfo(Bankinfo bankinfo) {
        this.bankinfo = bankinfo;
    }

    public Set<Sanction> getSanctions() {
        return sanctions;
    }

    public void setSanctions(Set<Sanction> sanctions) {
        this.sanctions = sanctions;
    }

    //endregion

}
