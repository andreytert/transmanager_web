package transmanager.entities;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
public class Route extends AbstractEntity {
    @OneToOne(cascade = CascadeType.ALL)
    private Location loadingAddress;
    @OneToOne(cascade = CascadeType.ALL)
    private Location unloadingAddress;
    private String name;

    public Route() {
        loadingAddress = new Location();
        unloadingAddress = new Location();
    }

    //region getters and setters
    public Location getLoadingAddress() {
        return loadingAddress;
    }

    public void setLoadingAddress(Location loadingAddress) {
        this.loadingAddress = loadingAddress;
    }

    public Location getUnloadingAddress() {
        return unloadingAddress;
    }

    public void setUnloadingAddress(Location unloadingAddress) {
        this.unloadingAddress = unloadingAddress;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    //endregion
}
