package transmanager.entities;

import javax.persistence.Entity;

@Entity
public class Location extends AbstractEntity {

    String fullAddress;

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }
}
